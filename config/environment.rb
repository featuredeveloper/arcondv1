# Load the Rails application.
require_relative 'application'

# Initialize the Rails application.
Rails.application.initialize!

# Added carrierwave to activerecord
require 'carrierwave/orm/activerecord'
