Rails.application.routes.draw do
  devise_for :users, :path_prefix => 'my'
  devise_scope :user do
    get '/login' => 'devise/sessions#new'
    get '/logout' => 'devise/sessions#destroy'
    get '/signup' => 'devise/registrations#new'
    get '/profile/update' => 'devise/registrations#edit'
    get '/profile/password' => 'devise/passwords#edit'
  end
  root 'home#index'
  resources :users, :controler => "user"
  resources :admins, :controler => "admin"
  post '/users/ativar/:id' => 'users#ativar', as: 'ativar'
  post '/users/desativar/:id' => 'users#desativar', as: 'desativar'
end
