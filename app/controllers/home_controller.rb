class HomeController < ApplicationController
  def index
    latitude = request.location.latitude
    longitude = request.location.longitude
    @users = User.where([:admin => false, :status => 'ativo']).near([latitude, longitude], 100000, :units => :km).paginate(page: params[:page], per_page: 20)
  end
end
