class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:username, :password, :confirm_password, :fullname, :celular, :telefone, :avatar, :avatar, :avatar_cache])
    devise_parameter_sanitizer.permit(:account_update, keys: [:username, :password, :confirm_password, :current_password, :fullname, :celular, :telefone, :avatar, :avatar, :avatar_cache, :remove_avatar])
  end
end
