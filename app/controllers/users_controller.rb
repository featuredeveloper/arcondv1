class UsersController < ApplicationController

  def index
    if current_user.admin
      respond_to do |format|
      format.html
      format.json{render json: UsersDatatable.new(view_context)}
    end
    else
      redirect_to root_path
    end
  end

  def show
      @user = User.find(params[:id])
  end

  def ativar
    if current_user.admin
      User.update((params[:id]), :status => 'ativo')
    else
      redirect_to root_path  
    end  
  end
  helper_method :ativar

  def desativar
    if current_user.admin
      User.update((params[:id]), :status => 'inativo')
    else
      redirect_to root_path
    end
  end

  helper_method :desativar

  def update
    if current_user.admin
      @user = User.find(params[:id])
      if params[:user][:password].blank?
        params[:user].delete(:password)
        params[:user].delete(:password_confirmation)
      end
      if @user.udapte(user_params)
        flash[:notice] = "Status alterado."
      else
        flash[:notice] = "Erro."
      end  
    else
      redirect_to root_path
    end
    
  end
  
  private

  def user_params
    params.require(:user).permit(:status)
  end
end
