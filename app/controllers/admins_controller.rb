class AdminsController < ApplicationController
  before_filter :verify_user

  def index
    @admins = User.where(admin: true).paginate(:page => params[:page])
  end

  def new
    @admin = User.new
  end

  def create  
    @admin = User.new(user_params)
    @admin.admin = true
    if @admin.save
      flash[:notice] = "Admin criado."
      redirect_to admins_path
    else
      render :new
    end
  end

  def edit
    @admin = User.find(params[:id]) 
  end

  def update
    @admin = User.find(params[:id])
    if @admin.update(user_params)
      flash[:notice] = "Usuario criado"
      redirect_to admins_path
    else
      render :edit
    end
  end

  def destroy
    @admin = User.find(params[:id])
    @admin.destroy
    redirect_to admins_path
  end

  protected

  def verify_user
    unless current_user.admin
      redirect_to root_path
    end
  end

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :admin)
  end
end
