class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  geocoded_by :address
  after_validation :geocode, :if => :address_changed?
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  mount_uploader :avatar, AvatarUploader

  validates_length_of :celular, within: 8..9, message: "Digite um numero de celular.", unless: :isAdmin
  validates_length_of :telefone, within: 8..9, message: "Digite um numero de telefone.", unless: :isAdmin

  private

  def isAdmin
    self.admin
  end  
end
