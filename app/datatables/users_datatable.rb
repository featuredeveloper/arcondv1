class UsersDatatable < AjaxDatatablesRails::Base
def_delegator :@view, :link_to
  def view_columns
    @view_columns ||= {
      username: { source: "User.username", cond: :like },
      fullname: { source: "User.fullname", cond: :like },
      email: { source: "User.email", cond: :like },
      celular: { source: "User.celular", cond: :like },
      telefone: { source: "User.telefone", cond: :like },
      status: { source: "User.status", cond: :like },
      edit: {source: 'editar'}
    }
  end

  def data
    records.map do |record|
      {
        username: link_to(record.username, @view.user_path(record)),
        fullname: record.fullname,
        email: record.email,
        celular: record.celular,
        telefone: record.telefone,
        status: record.status,
        editar:  record.status != 'ativo' ? link_to('Ativar', @view.ativar_path(record.id), :method => :post, class: 'btn btn-success btn-sm', remote: true) : link_to('Desativar', @view.desativar_path(record.id), :method => :post, :id => 'desativar', class: 'btn btn-danger btn-sm', remote: true)
      }
    end
  end

  

  private

  

  def get_raw_records
    User.where(:admin => false)
  end

  # ==== These methods represent the basic operations to perform on records
  # and feel free to override them

  # def filter_records(records)
  # end

  # def sort_records(records)
  # end

  # def paginate_records(records)
  # end

  # ==== Insert 'presenter'-like methods below if necessary
end
