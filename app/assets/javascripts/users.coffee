$(document).on "turbolinks:load", ->
    $('#users').dataTable
        serverSide: true
        processing: true
        sPaginationType: "full_numbers"
        ajax: $('#users').data('source')
        columns: [
            {data: 'username'}
            {data: 'fullname'}
            {data: 'email'}
            {data: 'celular'}
            {data: 'telefone'}
            {data: 'status'}
            {data: 'editar'}
        ]
    $("#users").bind "ajax:complete", ->
        $("#users").DataTable().ajax.reload()
    