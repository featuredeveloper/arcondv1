namespace :db do
    task populate: :environment do require 'faker'
        User.delete_all
        #Create 50 Users
        50.times do
            User.create! do |user|
                user.username = Faker::Internet.user_name
                user.fullname = Faker::Name.name
                user.email = Faker::Internet.email
                user.password = "password"
                user.celular = 999999999
                user.telefone = 55555555
                user.address = Faker::Address.full_address
                user.admin = false
            end
        end
        10.times do
            User.create! do |user|
                user.username = Faker::Internet.user_name
                user.fullname = Faker::Name.name
                user.email = Faker::Internet.email
                user.password = "password"
                user.celular = 999999999
                user.telefone = 55555555
                user.admin = true
            end
        end
    end
end