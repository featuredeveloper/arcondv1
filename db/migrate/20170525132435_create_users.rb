class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :username
      t.string :fullname
      t.string :status, default: "pendente", null: false
      t.float :latitude
      t.float :longitude
      t.integer :telefone
      t.integer :celular
      t.boolean :admin, default: false, null: false
      t.string :address

      t.timestamps
    end
  end
end
